"""
This program combines inference.py from assignment8 and classify_image.py mentioned in the lecture of week 7.  
It firstly runs car detection and location with ssd_mobilenet and crops the detected cars outlined by the obtained bounding boxes; 
and then runs image classification with Inception_v4 finetuned using the pj_vehicle image set on the check point provided 
It outputs human readable strings of the top 1 prediction along with the probability.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from PIL import Image

from utils import visualization_utils as vis_util
from utils import label_map_util

import argparse
import os.path
import re
import sys
import tarfile

from six.moves import urllib

if tf.__version__ < '1.4.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')

NUM_CLASSES = 1

class NodeLookup(object):
    """Converts integer node ID's to human readable labels."""

    def __init__(self, label_path=None):
        if not label_path:
            tf.logging.fatal('please specify the label file.')
            return
        self.node_lookup = self.load(label_path)

    def load(self, label_path):
        if not tf.gfile.Exists(label_path):
            tf.logging.fatal('File does not exist %s', lable_path)

        # Loads mapping from string UID to human-readable string
        proto_as_ascii_lines = tf.gfile.GFile(label_path).readlines()
        id_to_human = {}
        for line in proto_as_ascii_lines:
            if line.find(':') < 0:
                continue
            _id, human = line.rstrip('\n').split(':')
            id_to_human[int(_id)] = human

        return id_to_human
 
    def id_to_string(self, node_id):
        if node_id not in self.node_lookup:
            return ''
        return self.node_lookup[node_id]


def create_graph(model_file=None):
    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    if not model_file:
        model_file = PATH_TO_CLASSIFY_MODEL      
    with tf.gfile.FastGFile(model_file, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')

def run_inference_on_image(image, model_file=None):
    """Runs inference on an image.

    Args:
    image: Image file name.

    Returns:
    Nothing
    """

    if not tf.gfile.Exists(image):
        tf.logging.fatal('File does not exist %s', image)
    image_data = tf.gfile.FastGFile(image, 'rb').read()
    # Creates graph from saved GraphDef.
    create_graph(model_file)

    softmax_tensor = sess.graph.get_tensor_by_name('output:0')
    predictions = sess.run(softmax_tensor,
                           {'input:0': image_data})
    predictions = np.squeeze(predictions)

    # Creates node ID --> English string lookup.
    node_lookup = NodeLookup(PATH_TO_CLASSIFY_LABELS)

    top_k = predictions.argsort()[-FLAGS.num_top_predictions:][::-1]
    top_names = []
    scores = []
    for node_id in top_k:
        human_string = node_lookup.id_to_string(node_id)
        top_names.append(human_string)    
        score = predictions[node_id]
        scores.append(score)
        print('id:[%d] name:[%s] (score = %.5f)' % (node_id, human_string, score))
    return top_k, top_names, scores
    

def parse_args(check=True):
    parser = argparse.ArgumentParser()
    parser.add_argument('--output_dir', type=str, required=True)
    parser.add_argument('--dataset_dir', type=str, required=True)
    
    parser.add_argument('--num_top_predictions', type=int, default=1)
    parser.add_argument('--test_image', type=str, default='')


    FLAGS, unparsed = parser.parse_known_args()
    return FLAGS, unparsed


if __name__ == '__main__':
    FLAGS, unparsed = parse_args()
    
    PATH_TO_CLASSIFY_MODEL = os.path.join(FLAGS.dataset_dir, 'exported_graphs/frozen_classify_inference_graph.pb')
    PATH_TO_DETCTION_MODEL = os.path.join(FLAGS.dataset_dir, 'exported_graphs/frozen_detection_inference_graph.pb')
    PATH_TO_DETECTION_LABELS = os.path.join(FLAGS.dataset_dir, 'car_label.txt')
    PATH_TO_CLASSIFY_LABELS = os.path.join(FLAGS.dataset_dir, 'vehicle_labels.txt')
    
    if not FLAGS.test_image:    
        PATH_TO_TEST_IMAGE = os.path.join(FLAGS.dataset_dir, 'test.jpg')
    else:
        PATH_TO_TEST_IMAGE = FLAGS.test_image
    
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_DETCTION_MODEL, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map = label_map_util.load_labelmap(PATH_TO_DETECTION_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    def load_image_into_numpy_array(image):
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
            image = Image.open(PATH_TO_TEST_IMAGE)
            #image_data = tf.image.decode_jpeg(image)         
            
            image_np = load_image_into_numpy_array(image)
            image_np_expanded = np.expand_dims(image_np, axis=0)
            (boxes, scores, classes, num) = sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            score_tensor = np.squeeze(scores)
            box_tensor = np.squeeze(boxes)
            class_tensor = np.squeeze(classes)
            
            #find how many cars detected
            count = 0            
            for obj in score_tensor:
                if obj > 0.8:
                    count = count+1
               
            dis_str_list_list = []                    
            boxes = tf.slice(box_tensor, [0, 0], [count, 4]).eval() 
            boxes_shape = boxes.shape
            if not boxes_shape:
                raise ValueError('box shape unknown')
            if len(boxes_shape) != 2 or boxes_shape[1] != 4:
                raise ValueError('Input must be of size [N, 4]')
      
            width_original = image_np.shape[1]
            height_original = image_np.shape[0]
                   
            for i in range(count):
                dis_str_list = [] 
                (top, left, bottom, right) = (boxes[i, 0] * height_original, boxes[i, 1] * width_original, boxes[i, 2] * height_original, boxes[i, 3] * width_original)
                                          
                box = (left, top, right, bottom)
           
                roi=image.crop(box)
                roi_np = load_image_into_numpy_array(roi)

                plt.figure(i+1)
                name_str = 'test0'+str(i) + '.jpg' 
                plt.imshow(roi_np)
                plt.axis('off')
                plt.savefig(os.path.join(FLAGS.output_dir, name_str))                
                                
                car_id, car_name, pred_scores = run_inference_on_image(os.path.join(FLAGS.output_dir, name_str), PATH_TO_CLASSIFY_MODEL)

                dis_str_list.append(str(car_id[0])+': '+str(pred_scores[0])) 
                dis_str_list_list.append(dis_str_list) 
                
            vis_util.draw_bounding_boxes_on_image_array(image_np, boxes, display_str_list_list=dis_str_list_list)          
            plt.imsave(os.path.join(FLAGS.output_dir, 'output_car_detection.png'), image_np)
          
                        